<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Redis;


class StringAdder extends Component
{
    public $inputA;
    public $result;

    protected $rules = [
        'inputA' => 'required|string',
    ];


    public function addString()
    {
        $this->validate();

        $this->result = $this->result.'<br>'.$this->inputA;

        //Setter Redisa po skonfigurowaniu
        //Redis::set('resultString', $this->result);
        $this->inputA = '';
    }

    public function render()
    {
       //Getter redisa po skonfigurowaniu
       //$result = Redis::get('resultString') ?: ';

        $result = '';

       return view('livewire.string-adder',
            [
                'result' => $result
            ]
       );
    }
}
