<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Redis;
use Livewire\Component;

class Counter extends Component
{

    public $inputA;
    public $inputB;
    public $result;

    protected $rules = [
        'inputA' => 'required|numeric',
        'inputB' => 'required|numeric',
    ];


    public function count()
    {
        $this->validate();

        $this->result = $this->inputA + $this->inputB;
        //Setter Redisa po skonfigurowaniu
        //Redis::set('resultNumber', $this->result);
    }

    public function render()
    {
        //Getter redisa po skonfigurowaniu
        //$result = Redis::get('resultString') ?: '';

        $result = '';

        return view('livewire.counter',
            [
                'result' => $result
            ]);
    }
}
