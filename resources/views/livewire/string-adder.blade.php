<div style="text-align: center">
    <input style="border: 1px solid black" type="text" wire:model="inputA"/>
    @error('inputA') <span class="error">{{ $message }}</span> @enderror

    <button wire:click="addString">Dodaj string</button>
    <h1>{!! $result !!}</h1>
</div>
