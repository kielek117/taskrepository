<div style="text-align: center">
    <input style="border: 1px solid black" type="number" wire:model="inputA"/>
    @error('inputA') <span class="error">{{ $message }}</span> @enderror

    <input style="border: 1px solid black"  type="number" wire:model="inputB"/>
    @error('inputB') <span class="error">{{ $message }}</span> @enderror
    <button wire:click="count">Dodaj wartości</button>
    <h1>{{ $result }}</h1>
</div>
